package com.livecron.employees.api.output;

import java.util.Date;

import com.livecron.employees.api.model.EmployeeType;

public interface IEmployee {

    Long getId();

    Long getUserId();
    
    String getFirstName();

    String getLastName();

    Boolean getActive();
    
    EmployeeType getEmployeeType();

    Date getCreatedDate();

}
