package com.livecron.employees.api.model;

public enum EmployeeType {
	CEO,
    DIRECTOR_GENERAL,
    JEFE_DE_MARKETING,
    JEFE_DE_RECURSOS_HUMANOS,
    SECRETARIA,
    EMPLEADO
}

