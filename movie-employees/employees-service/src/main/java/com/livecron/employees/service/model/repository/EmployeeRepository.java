package com.livecron.employees.service.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.livecron.employees.service.model.domain.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{
	
}
