package com.livecron.employees.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.livecron.employees.service.model.domain.Employee;
import com.livecron.employees.service.model.repository.EmployeeRepository;


@Service
public class EmployeeGetAllService {

	@Autowired
    private EmployeeRepository employeeRepository;
	private List<Employee> employeeList;

	
	public void execute() {
		employeeList = employeeRepository.findAll();
	}
	
	public List<Employee> getAllEmployees() {
		return employeeList;
	}
}
