package com.livecron.employees.service.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.livecron.employees.api.input.EmployeeCreateInput;
import com.livecron.employees.api.model.EmployeeType;
import com.livecron.employees.service.model.domain.Employee;
import com.livecron.employees.service.model.repository.EmployeeRepository;

@Service
public class EmployeeCreateService {

	private EmployeeCreateInput input;
	
	private Employee employee;
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	public void execute() {
		Employee instance = composeEmployeeInstance();
		/*System.out.println(instance);
		System.out.println(instance.getFirstName() + " - " +
				instance.getLastName() + " - " +
				instance.getEmployeeType() + " - " +
				instance.getId() + " - " +
				instance.getCreatedDate() + " - " +
				instance.getActive());*/
		employee = employeeRepository.save(instance);
		System.out.println(employee);
		
	}
	
	public Employee composeEmployeeInstance() {
		EmployeeType type = readType();
		System.out.println(type);
		
		Employee instance = new Employee();
		instance.setFirstName(input.getFirstName());
		instance.setLastName(input.getLastName());
		instance.setActive(Boolean.TRUE);
		instance.setEmployeeType(type);
		instance.setUserId(input.getUserId());
        instance.setCreatedDate(new Date());
		
		return instance;
	}
	
	public EmployeeType readType() {
		System.out.println(input.getEmployeeType());
		switch(input.getEmployeeType()){
		case "CEO":
			System.out.println("ceo");
			return EmployeeType.CEO;
		case "DIRECTOR_GENERAL":
			System.out.println("director");
			return EmployeeType.DIRECTOR_GENERAL;
			
		case "JEFE_DE_MARKETING":
			System.out.println("jefe de marketing");
			return EmployeeType.JEFE_DE_MARKETING;
			
		case "JEFE_DE_RECURSOS_HUMANOS":
			System.out.println("jefe de recursos humanos");
			return EmployeeType.JEFE_DE_RECURSOS_HUMANOS;
			
		case "SECRETARIA":
			System.out.println("secretaria");
			return EmployeeType.SECRETARIA;
			
		case "EMPLEADO":
			System.out.println("empleado");
			return EmployeeType.EMPLEADO;
			
		default:
			System.out.println("default");
			return EmployeeType.EMPLEADO;
		}
	}
	
	public  void setInput(EmployeeCreateInput employeeCreateInput){
		System.out.println("set input !!");
		System.out.println(employeeCreateInput);
		this.input = employeeCreateInput;
	}
	
	public Employee getEmployee() {
		return employee;
	}
}
