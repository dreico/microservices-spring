package com.livecron.employees.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import com.livecron.employees.api.input.EmployeeCreateInput;
import com.livecron.employees.service.model.domain.Employee;
import com.livecron.employees.service.service.EmployeeCreateService;
import com.livecron.employees.service.service.EmployeeGetAllService;

@RequestMapping(value = "/employees")
@RequestScope
@RestController
public class EmployeeController {

	@Autowired
    private EmployeeCreateService employeeCreateService;
	
	@Autowired
    private EmployeeGetAllService employeeGetAllService;
	
	@RequestMapping(method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody EmployeeCreateInput input) {
        employeeCreateService.setInput(input);
        employeeCreateService.execute();

        return employeeCreateService.getEmployee();
    }
	
	@RequestMapping( method = RequestMethod.GET)
    public List<Employee> readAllTeachers() {
    	
        employeeGetAllService.execute();
        return employeeGetAllService.getAllEmployees();
    }
}
