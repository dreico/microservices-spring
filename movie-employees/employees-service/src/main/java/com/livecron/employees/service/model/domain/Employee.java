package com.livecron.employees.service.model.domain;

import org.hibernate.annotations.Type;

import com.livecron.employees.api.model.EmployeeType;
import com.livecron.employees.api.output.IEmployee;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Santiago Mamani
 */
@Entity
@Table(name = "employee_table")
public class Employee implements IEmployee{
    //ALT + ENTER
    // CTRL +ALt +L
	@Id
    @Column(name = "employeeid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
	
    @Column(name = "userid", nullable = false)
    private Long idUser;

    @Column(name = "firstname", length = 50, nullable = false)
    private String firstName;

    @Column(name = "lastanme", length = 50, nullable = false)
    private String lastName;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = "active", nullable = false)
    private Boolean active;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "type", length = 40, nullable = false)
    private EmployeeType type;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createddate", nullable = false, updatable = false)
    private Date createdDate;

    public Employee() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Long getUserId() {
    	return idUser;
    }
    
    public  void setUserId(Long idUser) {
    	this.idUser = idUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    
    public EmployeeType getEmployeeType() {
        return type;
    }

    public void setEmployeeType(EmployeeType type) {
        this.type = type;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}
